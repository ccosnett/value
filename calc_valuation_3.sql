use avm;

DROP PROCEDURE IF EXISTS calc_valuation_3;
DELIMITER $$
CREATE PROCEDURE calc_valuation_3(address_id INT)
-- This procedure takes in an address id and outputs the MVAL_base, Enh_MVAL_base and CF_percent_aggreg for that property
BEGIN

-- Declare variables used to store information on the target property and outputs of various intermediate calculations in the valuation
DECLARE p_type1, p_type2, p_type3, p_type4 VARCHAR(2);
DECLARE n_beds, num_comps_in_2km, number_val_components_used_ex_FS,  number_val_components_used_incl_FS, pd_type_flag_var,nhr INT;
DECLARE pc, street, epcownership, t_tenure, t_windows, t_epcrating, t_year_construction TEXT;
DECLARE t_lat, t_lon, search_radius, tfa DOUBLE;
DECLARE CF_INDSCOMP, CF_INDSM2COMP, CF_FSCOMP, CF_INDST_price, CF_weight_aggreg_ex_FS, CF_weight_aggreg, CF_FS_Price, CF_comp_weight_fixed, CF_comp_weight_add DOUBLE;
DECLARE CF_weight_FSCOMP, CF_weight_INDSCOMP, CF_weight_INDSM2COMP, CF_weight_INDST_price, CF_weight_FS_price, CF_percent_aggreg_data, CF_percent_aggreg DOUBLE;
DECLARE indexed_target_price DOUBLE; -- DECIMAL;
DECLARE Max_filter_mean_FS, Max_filter_mean_INDST,Max_filter_mean_INDSM2, Max_filter_mean_all DOUBLE;
DECLARE Val_FS, Val_INDSCOMP, Val_INDSM2COMP, Val_FSCOMP, Val_FSM2COMP,Val_INDST,Enh_val_INDST,Enh_val_INDSCOMP, Enh_val_INDSM2COMP, Enh_val_FSSCOMP,
		MVAL_base, Enh_MVAL_base DOUBLE;

# CF_INDSCOMP
# CF_weight_INDSCOMP

-- pull in data on the target property
SELECT
    postcode,
    thoroughfare,
    TYPE1,
    TYPE2,
    TYPE3,
    TYPE4,
    pd_type_flag, -- this is a flag indicating if the type data on the target comes from a PD source
    total_floor_area,
    Beds,
    latitude4326,
    longitude4326,
    EPC_ownership,
    CURRENT_ENERGY_RATING,
    tenure,
    windows,
    year_construction,
    (CF_data_quality_beds + CF_data_quality_beds + CF_data_quality_M2)/3, -- confidence factor on data quality of the target
	CASE
	 WHEN TYPE2='F' THEN
		  final_F_index/init_F_index * p
	 WHEN TYPE2='D' THEN
		  final_D_index/init_D_index * p
	 WHEN TYPE2='S' THEN
		  final_S_index/init_S_index * p
	 WHEN TYPE2='T' THEN
		  final_T_index/init_T_index * p
	 END AS indexed_price,
     number_habitable_rooms
INTO
	pc ,
	street ,
    p_type1 ,
    p_type2 ,
    p_type3 ,
    p_type4 ,
    pd_type_flag_var,
    tfa ,
    n_beds ,
    t_lat ,
    t_lon,
	epcownership ,
    t_epcrating ,
    t_tenure ,
    t_windows ,
    t_year_construction,
    CF_percent_aggreg_data,
    indexed_target_price, nhr
FROM
    (
    SELECT
        ah.aid,
		postcode,
		propTypeCategory,
		EPC_ownership,
        thoroughfare,
        lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
        lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
		ind.price AS p,

        CASE WHEN ah.propertyType IS NULL THEN 0 ELSE 1 END AS pd_type_flag,

		CASE WHEN ah.propertyType IS NOT NULL THEN 1
			 WHEN COALESCE(NP_SUBBNAME_F_type2_F, NP_NB1_D_PC_type2_D, NP_NLH1_TM_PC_type2_T) IS NOT NULL THEN 0.4
			 WHEN NP_PD_SUMM0_TO_6_type3_PC IS NOT NULL THEN 0.3
			 ELSE 0.1 END AS CF_data_quality_type,

	   CASE WHEN NUMBER_HABITABLE_ROOMS IS NOT NULL THEN 1
			-- when EA/PORT data is available then 0.5
			WHEN NNPAV_NHR_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_beds,

	   CASE WHEN TOTAL_FLOOR_AREA IS NOT NULL THEN 1
			WHEN NNPAV_M2_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_M2,

	   CASE WHEN ah.propertyType IS NULL THEN NP_PD_SUMM0_TO_9_type4_PC ELSE ah.propertyType END AS pType,
		CASE
			WHEN ten.UD IS NOT NULL THEN ten.UD
			WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP
			ELSE ten.AH
		END AS tenure,
		CASE
			WHEN window_desc.UD IS NOT NULL THEN window_desc.UD
			ELSE window_desc.EPC
		END AS windows,
		ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
		COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
		Indic_year_band_construction AS year_construction,
		CURRENT_ENERGY_RATING,
		latitude4326,
		longitude4326
    FROM
        liqquid_addresshub ah
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
    LEFT JOIN avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
    LEFT JOIN nn_nhr nnpav_nhr ON ah.aid = nnpav_nhr.aid
    LEFT JOIN nn_type nnpav_p ON ah.aid = nnpav_p.aid
    LEFT JOIN nn_m2 nnpav_m2 ON ah.aid = nnpav_m2.aid
    LEFT JOIN avm_tenure ten ON ah.aid = ten.aid
    LEFT JOIN avm_window_description window_desc ON ah.aid = window_desc.aid
    LEFT JOIN avm_year_construction yc ON ah.aid = yc.aid
    ) s
	LEFT JOIN avm_property_type_matrix ptm ON s.pType = ptm.Type4
    LEFT JOIN liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
WHERE
    aid = address_id;


/*SELECT pc ,
	street ,
    p_type1 ,
    p_type2 ,
    p_type3 ,

    p_type4 ,
    pd_type_flag_var,
    tfa ,
    n_beds ,
    t_lat ,
    t_lon,
	epcownership ,
    t_epcrating ,
    t_tenure ,
    t_windows ,
    t_year_construction,
    CF_percent_aggreg_data,
    indexed_target_price, nhr;*/

-- =========================================================================================================================================================================

-- this section of the script pulls in the nearest postcodes to the target
-- it works by first estimating the density of appropriate comparables in a box with 2km side length centreed on the target property
-- the 2km box is then scaled to contain approximately 250 houses
DROP TEMPORARY TABLE IF EXISTS nearest_postcodes;
CREATE TEMPORARY TABLE nearest_postcodes (postcode VARCHAR(8));

SET num_comps_in_2km = IFNULL((SELECT SUM(number_of_houses) FROM avm_postcode_type_price_stats_2 WHERE MBRContains(LineString(Point(t_lon + 0.018, t_lat + 0.018), Point(t_lon - 0.018, t_lat - 0.018)), coords)
					AND type1=p_type1 AND Beds = n_beds),0);

IF num_comps_in_2km = 0 THEN
	SET search_radius = 0.1;
ELSEIF  num_comps_in_2km > 200 THEN
	SET search_radius = 0.018;
ELSE
	SET search_radius = LEAST((250/num_comps_in_2km)*0.018,0.1);
END IF;

INSERT INTO nearest_postcodes SELECT
    postcode
FROM
    (
    SELECT
        postcode
    FROM
        avm_postcode_type_price_stats_2
    WHERE
        MBRCONTAINS(LINESTRING(POINT(t_lon + search_radius, t_lat + search_radius), POINT(t_lon - search_radius, t_lat - search_radius)), coords)
	AND Beds = n_beds
	AND type1 = p_type1
	) t;








############################ Underlying ############################ Underlying ############################ Underlying
############################ Underlying ############################ Underlying ############################ Underlying
/*
SELECT          *
							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((t_lat - latitude4326)*70.25,2) + POWER((t_lon - longitude4326)*42.08,2))*1609.344 AS distance,
									EPC_ownership,
									CURRENT_ENERGY_RATING,
									pp.TransactionDate,
									postcode,
									thoroughfare,
									pp.propertyType AS pp_pType,
									ah.propertyType AS ah_pType,
									CASE WHEN ten.UD IS NOT NULL THEN ten.UD WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP ELSE AH END AS tenure,
									CASE WHEN window_desc.UD IS NOT NULL THEN window_desc.UD ELSE EPC END AS windows,
									NUMBER_HABITABLE_ROOMS AS nhr,
									TOTAL_FLOOR_AREA AS total_floor_area,
									Indic_year_band_construction AS year_construction,
									lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
									lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
									lind.latest_index AS final_O_index, ind.prop_index AS init_O_index,
									ind.price AS p,
									coords
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN (SELECT postcode FROM nearest_postcodes)
								) s
							LEFT JOIN
								avm_property_type_matrix ptm_pp ON s.pp_pType = ptm_pp.Type4
							LEFT JOIN
								avm_property_type_matrix ptm_ah ON s.ah_pType = ptm_ah.Type4
							LEFT JOIN
								liqquid_hab_rooms_est lhre ON s.nhr = lhre.Habitable_Rooms
							WHERE
								Beds=n_beds
							AND
								ptm_ah.TYPE1=p_type1 AND
								p IS NOT NULL AND aid <> address_id
							ORDER BY distance
							LIMIT 100;
SELECT 'a';

 */
############################ Underlying ############################ Underlying ############################ Underlying
############################ Underlying ############################ Underlying ############################ Underlying

-- this section of the script pulls in all the relevant INDS comparables and calculates their distance, attribute and transaction timing points
DROP TEMPORARY TABLE IF EXISTS comps_INDS;
CREATE TEMPORARY TABLE comps_INDS (aid INT, address1line TEXT, has_epc_m2 INT, distance_points DOUBLE, epc_ownership_points INT, transac_timing_points DOUBLE,
								   tenure_points INT, window_points INT, M2_points DOUBLE, EPC_rating_points INT, year_construction_points INT, type_points INT,
                                   price DOUBLE, total_floor_area DOUBLE, rank INT);

/*SET @rank=0;*/
INSERT INTO comps_INDS SELECT
						*, 1 AS rank
					   FROM
							(SELECT
								aid,
								address1line,
								CASE WHEN TOTAL_FLOOR_AREA=0 THEN 0 ELSE 1 END AS has_epc_m2, -- flag indicating presence of EPC m2 data
								(POWER(POWER(GREATEST( (distance/1609.344),(1/15) ),2), -1) + 50*(CASE WHEN thoroughfare=street THEN 1 ELSE 0 END) + 50*(CASE WHEN postcode=pc THEN 1 ELSE 0 END)) AS distance_points,
                                (30*(CASE WHEN EPC_ownership=epcownership THEN 1 ELSE 0 END)) AS EPC_ownership_points,
                                GREATEST((1-DATEDIFF(CURDATE(),TransactionDate)/3650)*300,0) AS transac_timing_points,
                                IFNULL((SELECT points FROM avm_tenure_att_points WHERE tenure_of_target = t_tenure and tenure_of_comp=tenure),0) AS tenure_points,
                                IFNULL((SELECT points FROM avm_window_description_points WHERE target_windows_desc = t_windows and comp_windows_desc=windows),0) AS windows_points,
                                CASE WHEN ABS(1-( tfa / total_floor_area )) > 0.2 THEN 0 ELSE ((( 0.2 - ABS(1-( tfa / total_floor_area ))) / 0.2 ) * 100) END AS M2_points,
								IFNULL((SELECT points FROM avm_EPC_rating_points WHERE target_EPC_rating = t_epcrating and comp_EPC_rating=CURRENT_ENERGY_RATING),0) AS EPC_rating_points,
								IFNULL((SELECT points FROM avm_year_construction_points WHERE target_year_construction = t_year_construction and comp_year_construction=year_construction),0) AS year_construction_points,
								CASE WHEN pd_type_flag_var = 1 THEN CASE WHEN ptm_ah.TYPE4=p_type4 THEN 100 WHEN ptm_ah.TYPE3=p_type3 THEN 80 WHEN ptm_ah.TYPE2=p_type2 THEN 75 WHEN ptm_ah.TYPE1=p_type1 THEN 0 ELSE 0 END ELSE 0 END AS type_points,
							    total_floor_area,
								CASE
								   WHEN ptm_pp.TYPE2='F' THEN
									    final_F_index/init_F_index * p
								   WHEN ptm_pp.TYPE2='D' THEN
									    final_D_index/init_D_index * p
								   WHEN ptm_pp.TYPE2='S' THEN
									    final_S_index/init_S_index * p
								   WHEN ptm_pp.TYPE2='T' THEN
									    final_T_index/init_T_index * p
								   WHEN ptm_pp.TYPE2='O' THEN
									    final_O_index/init_O_index * p
								   END AS indexed_price


							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((t_lat - latitude4326)*70.25,2) + POWER((t_lon - longitude4326)*42.08,2))*1609.344 AS distance,
									EPC_ownership,
									CURRENT_ENERGY_RATING,
									pp.TransactionDate,
									postcode,
									thoroughfare,
									pp.propertyType AS pp_pType,
									ah.propertyType AS ah_pType,
									CASE WHEN ten.UD IS NOT NULL THEN ten.UD WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP ELSE AH END AS tenure,
									CASE WHEN window_desc.UD IS NOT NULL THEN window_desc.UD ELSE EPC END AS windows,
									NUMBER_HABITABLE_ROOMS AS nhr,
									TOTAL_FLOOR_AREA AS total_floor_area,
									Indic_year_band_construction AS year_construction,
									lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
									lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
									lind.latest_index AS final_O_index, ind.prop_index AS init_O_index,
									ind.price AS p,
									coords
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN (SELECT postcode FROM nearest_postcodes)
								) s
							LEFT JOIN
								avm_property_type_matrix ptm_pp ON s.pp_pType = ptm_pp.Type4
							LEFT JOIN
								avm_property_type_matrix ptm_ah ON s.ah_pType = ptm_ah.Type4
							LEFT JOIN
								liqquid_hab_rooms_est lhre ON s.nhr = lhre.Habitable_Rooms
							WHERE
								Beds=n_beds
							AND
								ptm_ah.TYPE1=p_type1 AND
								p IS NOT NULL AND aid <> address_id
							ORDER BY distance
							LIMIT 100) p;

SELECT * FROM comps_INDS;
#SELECT * FROM points_table_INDS;


DROP TEMPORARY TABLE IF EXISTS points_table_INDS;
CREATE TEMPORARY TABLE points_table_INDS (rank INT, aid INT, has_EPC_M2 INT, attribute_points DOUBLE, distance_points DOUBLE, timing_points DOUBLE, total_points DOUBLE, total_floor_area DOUBLE, price DOUBLE);
INSERT INTO
	points_table_INDS
	SELECT
		rank,
		aid,
		has_EPC_M2,
		type_points + year_construction_points + window_points + tenure_points + M2_points + EPC_rating_points + EPC_ownership_points,
		distance_points,
		transac_timing_points,
		type_points + year_construction_points + window_points + tenure_points + M2_points + EPC_rating_points + EPC_ownership_points + distance_points + transac_timing_points,
		price,
		total_floor_area
	FROM
		comps_INDS;



SET CF_INDSCOMP = (SELECT SUM(total_points)/(20*945) FROM points_table_INDS WHERE rank <=20); -- confidence factor for indexed sold comparables
SET CF_INDSM2COMP = (SELECT SUM(total_points)/(20*945) FROM points_table_INDS WHERE has_EPC_M2 =1 AND rank <=20);   -- confidence factor for indexed sold comparables on a per m2 basis




SET CF_INDST_price = (
					SELECT
						CASE WHEN TransactionDate IS NULL THEN 0 ELSE GREATEST((1-DATEDIFF(CURDATE(),TransactionDate)/3650),0) END
					FROM
						liqquid_addresshub ah
					LEFT JOIN
						liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
					WHERE ah.aid = address_id
                    );

-- =========================================================================================================================================================================
-- this section of the script pulls in all the relevant FS comparables and calculates their distance, attribute and transaction timing points
DROP TEMPORARY TABLE IF EXISTS comps_FS;
CREATE TEMPORARY TABLE comps_FS (id INT, address TEXT,distance_points DOUBLE,
                                 type_points INT, CF_data_quality_type INT, price INT,rank INT);

/*SET @rank=0;*/
INSERT INTO comps_FS SELECT
*, 1 AS rank
FROM
(SELECT
id,
address,
(POWER(POWER(GREATEST( (distance/1609.344),(1/15) ),2), -1) + 50*(CASE WHEN thoroughfare=street THEN 1 ELSE 0 END) + 50*(CASE WHEN postcode=pc THEN 1 ELSE 0 END)) AS distance_points,
CASE WHEN pd_type_flag_var = 1 THEN CASE WHEN TYPE4=p_type4 THEN 225 WHEN TYPE3=p_type3 THEN 225 WHEN TYPE2=p_type2 THEN 225 WHEN TYPE1=p_type1 THEN 0 ELSE 0 END ELSE 0 END AS type_points,
4 AS CF_data_quality_type,
Price
FROM
(SELECT SQRT(POWER((t_lat - lat)*70.25,2) + POWER((t_lon - lon)*42.08,2))*1609.344 AS distance, id,address, TYPE1, TYPE2, TYPE3, TYPE4, postcode, thoroughfare, Price
FROM liqquid_EA_PORT_data WHERE MBRContains(LineString(Point(t_lon + 0.036, t_lat + 0.036), Point(t_lon - 0.036, t_lat - 0.036)), coords)
AND  TYPE1=p_type1 AND bedrooms = n_beds AND price <> 'POA' AND aid <> address_id) s
ORDER BY distance LIMIT 20) p;

#SELECT * FROM comps_FS;

DROP TEMPORARY TABLE IF EXISTS points_table_FS;
CREATE TEMPORARY TABLE points_table_FS (rank INT, attribute_points DOUBLE, distance_points DOUBLE, total_points DOUBLE, price INT);
INSERT INTO
	points_table_FS
		SELECT
			rank,
			type_points,
			distance_points,
			type_points + distance_points,
			Price
		FROM comps_FS;


#SELECT * FROM points_table_FS;

SET CF_FSCOMP = (SELECT SUM(total_points)/(20*(550)) FROM points_table_FS WHERE rank <=20);		-- CF_FSCOMP

--  =============================================================================================================================================================================

-- this section of the script calculates the confidence factors to weight the individual valuation components
-- excluding FS_PRICE
SET number_val_components_used_ex_FS = (SELECT CASE WHEN CF_FSCOMP IS NULL THEN 0 ELSE 1 END +
                                               CASE WHEN CF_INDST_price IS NULL THEN 0 ELSE 1  END +
                                               CASE WHEN CF_INDSCOMP IS NULL THEN 0 ELSE 1 END +
                                               CASE WHEN CF_INDSM2COMP IS NULL THEN 0 ELSE 1 END);

SET CF_weight_aggreg_ex_FS = (((CF_FSCOMP + CF_INDST_price + CF_INDSCOMP + CF_INDSM2COMP)/number_val_components_used_ex_FS)*0.7 + (number_val_components_used_ex_FS/4)*0.3);


SET CF_FS_Price = 0; -- For the moment we dont use Val_FS until we have certainty of match on user data (SELECT LEAST(CF_weight_aggreg_ex_FS + 0.2, 0.8));

SET number_val_components_used_incl_FS = (SELECT number_val_components_used_ex_FS + CASE WHEN CF_FS_PRICE = 0 THEN 0 ELSE 1 END);

SET CF_weight_aggreg = (SELECT ((CF_FSCOMP + CF_INDST_price + CF_INDSCOMP + CF_INDSM2COMP + CF_FS_Price)/number_val_components_used_incl_FS)*0.7 + (number_val_components_used_incl_FS/4)*0.3);

-- =====================================================================================================================================================================================
-- calculation of the valuations components
SET Val_INDST = IFNULL(indexed_target_price,0);
SET Val_FS = 0; -- IFNULL((SELECT price FROM liqquid_EA_PORT_data WHERE aid = address_id),0);
SET Val_INDSCOMP = (SELECT SUM(total_points*price)/SUM(total_points) FROM points_table_INDS);
SET Val_INDSM2COMP  = (SELECT SUM(total_points*(price/total_floor_area))/SUM(total_points) FROM (SELECT * FROM points_table_INDS WHERE has_EPC_M2 = 1) s )*tfa;
SET Val_FSCOMP =  (SELECT SUM(total_points*price)/SUM(total_points) FROM points_table_FS WHERE rank <=20);


SET CF_comp_weight_fixed = 0.3/6;
SET CF_comp_weight_add = 1 - CF_comp_weight_fixed;

SELECT
	   CF_comp_weight_fixed + (CF_FSCOMP*CF_comp_weight_add) AS CF_weight_FSCOMP,
	   CF_comp_weight_fixed + (CF_INDSCOMP*CF_comp_weight_add) AS CF_weight_INDSCOMP,
	   CF_comp_weight_fixed + (CF_INDSM2COMP*CF_comp_weight_add) AS CF_weight_INDSM2COMP,
       CASE WHEN Val_INDST = 0 THEN 0 ELSE CF_comp_weight_fixed + (CF_INDST_price*CF_comp_weight_add) END AS CF_weight_INDST_price,
	   CASE WHEN Val_FS = 0 THEN 0 ELSE CF_comp_weight_fixed + (CF_FS_price*CF_comp_weight_add) END AS CF_weight_FS_price
       INTO
       CF_weight_FSCOMP,
       CF_weight_INDSCOMP,
       CF_weight_INDSM2COMP,
       CF_weight_INDST_price,
       CF_weight_FS_price;


-- here we calculate the price caps for each of the valuation components
#CALL Max_Enhanced_Prices(TRUE, FALSE, pc, n_beds, t_lat, t_lon, p_type1,p_type2, p_type3, @filterval);
#SET Max_filter_mean_FS = @filterval;
#CALL Max_Enhanced_Prices(FALSE, FALSE, pc, n_beds, t_lat, t_lon, p_type1,p_type2, p_type3, @filterval);
#SET Max_filter_mean_INDST = @filterval;
#CALL Max_Enhanced_Prices(FALSE, FALSE, pc, n_beds, t_lat, t_lon, p_type1,p_type2, p_type3, @filterval);
#SET Max_filter_mean_INDSM2 = (indexed_target_price/tfa)*@filterval;
#SET Max_filter_mean_all = IFNULL((Max_filter_mean_FS + Max_filter_mean_INDST + Max_filter_mean_INDSM2)/3,0);


#SET Enh_val_INDST = (SELECT GREATEST(Val_INDST + IFNULL((SELECT Total_all_enhance_cost_net FROM avm_cost_enhancements),0), Max_filter_mean_all));
#SET Enh_val_INDSCOMP = (SELECT GREATEST(Val_INDSCOMP +  IFNULL((SELECT Total_imp_enhance_cost_net FROM avm_cost_enhancements),0), Max_filter_mean_all));
#SET Enh_val_INDSM2COMP = (SELECT GREATEST(Val_INDSM2COMP +  IFNULL((SELECT Total_imp_enhance_cost_net FROM avm_cost_enhancements),0), Max_filter_mean_all));
#SET Enh_val_FSSCOMP = (SELECT GREATEST(Val_FSCOMP +  IFNULL((SELECT Total_imp_enhance_cost_net FROM avm_cost_enhancements),0), Max_filter_mean_all));

SET MVAL_base = (Val_INDST*CF_weight_INDST_price + Val_INDSCOMP*CF_weight_INDSCOMP + Val_INDSM2COMP*CF_weight_INDSM2COMP + Val_FS*CF_weight_FS_price + Val_FSCOMP* CF_weight_FSCOMP)/				(CF_weight_INDST_price + CF_weight_INDSCOMP + CF_weight_INDSM2COMP + CF_weight_FS_price + CF_weight_FSCOMP);

#SET Enh_MVAL_base = (Enh_val_INDST*CF_INDST_price + Enh_val_INDSCOMP*CF_INDSCOMP + Enh_val_INDSM2COMP*CF_INDSM2COMP + Val_FS*CF_FS_price + Val_FSCOMP* CF_weight_FSCOMP)/				(CF_INDST_price + CF_INDSCOMP + CF_INDSM2COMP + CF_FS_price + CF_weight_FSCOMP);

SET CF_percent_aggreg = CF_percent_aggreg_data * CF_weight_aggreg;


INSERT INTO aid_cache_2 (aid) VALUES (address_id);
INSERT INTO cache_valuations_2 (aid, MVAL_base,Enh_MVAL_base, CF_percent_aggreg) VALUES (address_id, MVAL_base, Enh_MVAL_base, CF_percent_aggreg);
#SELECT MVAL_base, Enh_MVAL_base, CF_percent_aggreg;
END$$
DELIMITER ;

CALL calc_valuation_3(21813098);

SELECT * from information_schema.PROCESSLIST;

SELECT SLEEP(10);