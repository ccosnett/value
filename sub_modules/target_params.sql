SELECT
    postcode,
    thoroughfare,
    TYPE1,
    TYPE2,
    TYPE3,
    TYPE4,
    pd_type_flag, -- this is a flag indicating if the type data on the target comes from a PD source
    total_floor_area,
    Beds,
    latitude4326,
    longitude4326,
    EPC_ownership,
    CURRENT_ENERGY_RATING,
    tenure,
    windows,
    year_construction,
    (CF_data_quality_beds + CF_data_quality_beds + CF_data_quality_M2)/3, -- confidence factor on data quality of the target
	CASE
	 WHEN TYPE2='F' THEN
		  final_F_index/init_F_index * p
	 WHEN TYPE2='D' THEN
		  final_D_index/init_D_index * p
	 WHEN TYPE2='S' THEN
		  final_S_index/init_S_index * p
	 WHEN TYPE2='T' THEN
		  final_T_index/init_T_index * p
	 END AS indexed_price,
     number_habitable_rooms
FROM
    (
    SELECT
        ah.aid,
		postcode,
		propTypeCategory,
		EPC_ownership,
        thoroughfare,
        lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
        lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
		ind.price AS p,

        CASE WHEN ah.propertyType IS NULL THEN 0 ELSE 1 END AS pd_type_flag,

		CASE WHEN ah.propertyType IS NOT NULL THEN 1
			 WHEN COALESCE(NP_SUBBNAME_F_type2_F, NP_NB1_D_PC_type2_D, NP_NLH1_TM_PC_type2_T) IS NOT NULL THEN 0.4
			 WHEN NP_PD_SUMM0_TO_6_type3_PC IS NOT NULL THEN 0.3
			 ELSE 0.1 END AS CF_data_quality_type,

	   CASE WHEN NUMBER_HABITABLE_ROOMS IS NOT NULL THEN 1
			-- when EA/PORT data is available then 0.5
			WHEN NNPAV_NHR_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_beds,

	   CASE WHEN TOTAL_FLOOR_AREA IS NOT NULL THEN 1
			WHEN NNPAV_M2_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_M2,

	   CASE WHEN ah.propertyType IS NULL THEN NP_PD_SUMM0_TO_9_type4_PC ELSE ah.propertyType END AS pType,
		CASE
			WHEN ten.UD IS NOT NULL THEN ten.UD
			WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP
			ELSE ten.AH
		END AS tenure,
		CASE
			WHEN window_desc.UD IS NOT NULL THEN window_desc.UD
			ELSE window_desc.EPC
		END AS windows,
		ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
		COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
		Indic_year_band_construction AS year_construction,
		CURRENT_ENERGY_RATING,
		latitude4326,
		longitude4326
    FROM
        liqquid_addresshub ah
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
    LEFT JOIN avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
    LEFT JOIN NNPAV_NHR_2 nnpav_nhr ON ah.aid = nnpav_nhr.aid
    LEFT JOIN NNPAV_PropertyType2 nnpav_p ON ah.aid = nnpav_p.aid
    LEFT JOIN NNPAV_M2_2 nnpav_m2 ON ah.aid = nnpav_m2.aid
    LEFT JOIN avm_tenure_old_dupe ten ON ah.aid = ten.aid
    LEFT JOIN avm_window_description window_desc ON ah.aid = window_desc.aid
    LEFT JOIN avm_year_construction yc ON ah.aid = yc.aid
    ) s
	LEFT JOIN avm_property_type_matrix ptm ON s.pType = ptm.Type4
    LEFT JOIN liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
WHERE
    aid = 21813098
LIMIT 1;





    SELECT
        ah.aid,
		postcode,
		propTypeCategory,
		EPC_ownership,
        thoroughfare,
        lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
        lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
		ind.price AS p,

        CASE WHEN ah.propertyType IS NULL THEN 0 ELSE 1 END AS pd_type_flag,

		CASE WHEN ah.propertyType IS NOT NULL THEN 1
			 WHEN COALESCE(NP_SUBBNAME_F_type2_F, NP_NB1_D_PC_type2_D, NP_NLH1_TM_PC_type2_T) IS NOT NULL THEN 0.4
			 WHEN NP_PD_SUMM0_TO_6_type3_PC IS NOT NULL THEN 0.3
			 ELSE 0.1 END AS CF_data_quality_type,

	   CASE WHEN NUMBER_HABITABLE_ROOMS IS NOT NULL THEN 1
			-- when EA/PORT data is available then 0.5
			WHEN NNPAV_NHR_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_beds,

	   CASE WHEN TOTAL_FLOOR_AREA IS NOT NULL THEN 1
			WHEN NNPAV_M2_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_M2,

	   CASE WHEN ah.propertyType IS NULL THEN NP_PD_SUMM0_TO_9_type4_PC ELSE ah.propertyType END AS pType,
		CASE
			WHEN ten.UD IS NOT NULL THEN ten.UD
			WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP
			ELSE ten.AH
		END AS tenure,
		CASE
			WHEN window_desc.UD IS NOT NULL THEN window_desc.UD
			ELSE window_desc.EPC
		END AS windows,
		ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
		COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
		Indic_year_band_construction AS year_construction,
		CURRENT_ENERGY_RATING,
		latitude4326,
		longitude4326
    FROM
        liqquid_addresshub ah
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
    LEFT JOIN avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
    LEFT JOIN NNPAV_NHR_2 nnpav_nhr ON ah.aid = nnpav_nhr.aid
    LEFT JOIN NNPAV_PropertyType2 nnpav_p ON ah.aid = nnpav_p.aid
    LEFT JOIN NNPAV_M2_2 nnpav_m2 ON ah.aid = nnpav_m2.aid
    LEFT JOIN avm_tenure_old_dupe ten ON ah.aid = ten.aid
    LEFT JOIN avm_window_description window_desc ON ah.aid = window_desc.aid
    LEFT JOIN avm_year_construction yc ON ah.aid = yc.aid
    WHERE ah.aid = 21813098;