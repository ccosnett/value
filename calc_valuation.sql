
############################################################################################################
############################################################################################################
DROP PROCEDURE IF EXISTS calc_valuation;
DELIMITER $$
CREATE PROCEDURE calc_valuation(IN address_id INT)
BEGIN

DECLARE mvalBase DOUBLE;

IF address_id IN (SELECT * FROM aid_cache)
    THEN (SELECT MVAL_base, Enh_MVAL_base, CF_percent_aggreg from cache_valuations WHERE aid=address_id LIMIT 1);
ELSE CALL calc_valuation_2(address_id);
    END IF;
#SELECT MVAL_base, Enh_MVAL_base, CF_percent_aggreg;
END$$
DELIMITER ;
############################################################################################################
############################################################################################################
