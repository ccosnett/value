USE valdev_4;
SELECT aid, TYPE1, TYPE2, TYPE3, TYPE4, Beds, postcode, coords, total_floor_area,
	CASE WHEN pType="F" THEN
				latest_F/F * PRICE
                WHEN pType="D" THEN
                latest_D/D * PRICE
                WHEN pType="S" THEN
                latest_S/S * PRICE
                WHEN pType="T" THEN
                latest_T/T * PRICE
                END AS indexed_price
FROM 
(SELECT ah.aid,
			CASE
				WHEN
					propTypeCategory IS NULL
						AND ah.propertyType = ''
				THEN
					NP_PD_SUMM0_TO_9_type4_PC
				ELSE ah.propertyType
			END AS pType,
            postcode,
            pp.TransactionDate,
            latest_F,F, latest_D,D,latest_T, T, latest_S, S,
            ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
            COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
            pp.Price AS PRICE,
            coords
		FROM
			liqquid_addresshub ah
			INNER JOIN liqquid_pp pp ON ah.aid = pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
            INNER JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
			LEFT JOIN NNPAV_NHR nn_nhr ON ah.aid = nn_nhr.aid
			LEFT JOIN NNPAV_PropertyType nn_p ON ah.aid = nn_p.aid
			LEFT JOIN NNPAV_M2 nn_m2 ON ah.aid = nn_m2.aid
            LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
		) s
        LEFT JOIN
    avm_property_type_matrix ptm ON s.pType = ptm.Type4
        LEFT JOIN
    liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
    WHERE PRICE IS NOT NULL;


SELECT * FROM max_enhanced_price_view_pp;
SELECT * FROM information_schema.PROCESSLIST WHERE DB='valdev_4'
SELECT CONCAT('KILL ',id,';') FROM information_schema.PROCESSLIST;
KILL 5598131;
KILL 5598134;
KILL 35250482;
KILL 35250449;
KILL 35250484;
KILL 5598132;
KILL 35250450;
KILL 35250478;
KILL 35250475;
KILL 35250480;
KILL 33457570;

"CONCAT('KILL ',id,';')"
KILL 5598131;
KILL 5598134;
KILL 35250488;
KILL 5598132;
KILL 35250489;
KILL 35250491;
KILL 35250485;
KILL 33457570;

"CONCAT('KILL ',id,';')"
KILL 5598131;
KILL 5598134;
KILL 5598132;
KILL 35250492;
KILL 35250493;
KILL 33457570;

"CONCAT('KILL ',id,';')"
KILL 5598131;
KILL 5598134;
KILL 35250499;
KILL 5598132;
KILL 35250501;
KILL 35250508;
KILL 35250502;
KILL 35250495;
KILL 35250503;
KILL 35250504;
KILL 35250505;
KILL 35250506;
KILL 33457570;


CREATE TABLE NNPAV_PropertyType3 LIKE nn_type;
INSERT NNPAV_PropertyType3 SELECT * FROM nn_type;