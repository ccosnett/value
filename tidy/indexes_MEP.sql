## {4} liqquid_pp

alter table liqquid_pp	add primary key (aid, TransactionId);
create index aid on liqquid_pp (aid);
create index district on liqquid_pp (District);
create index txid on liqquid_pp (TransactionId);
CREATE INDEX PRICE_INDEX ON liqquid_pp(Price);

alter table NNPAV_NHR add primary key (aid);
alter table NNPAV_PropertyType add primary key (aid);
alter table NNPAV_M2 add primary key (aid);
SELECT * FROM NNPAV_NHR;

