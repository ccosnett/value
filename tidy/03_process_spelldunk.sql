use valdev;
DROP PROCEDURE IF EXISTS proc;
DELIMITER $$
CREATE PROCEDURE proc(IN address_id INT)
-- This procedure takes in an address id and outputs the MVAL_base, Enh_MVAL_base and CF_percent_aggreg for that property
BEGIN

-- Declare variables used to store information on the target property and outputs of various intermediate calculations in the valuation
DECLARE p_type1, p_type2, p_type3, p_type4 VARCHAR(2);
DECLARE n_beds, num_comps_in_2km, number_val_components_used_ex_FS,  number_val_components_used_incl_FS, pd_type_flag_var,nhr INT;
DECLARE pc, street, epcownership, t_tenure, t_windows, t_epcrating, t_year_construction TEXT;
DECLARE t_lat, t_lon, search_radius, tfa DOUBLE;
DECLARE CF_INDSCOMP, CF_INDSM2COMP, CF_FSCOMP, CF_INDST_price, CF_weight_aggreg_ex_FS, CF_weight_aggreg, CF_FS_Price, CF_comp_weight_fixed, CF_comp_weight_add DOUBLE;
DECLARE CF_weight_FSCOMP, CF_weight_INDSCOMP, CF_weight_INDSM2COMP, CF_weight_INDST_price, CF_weight_FS_price, CF_percent_aggreg_data, CF_percent_aggreg DOUBLE;
DECLARE indexed_target_price DOUBLE;
DECLARE Max_filter_mean_FS, Max_filter_mean_INDST,Max_filter_mean_INDSM2, Max_filter_mean_all DOUBLE;
DECLARE Val_FS, Val_INDSCOMP, Val_INDSM2COMP, Val_FSCOMP, Val_FSM2COMP,Val_INDST,Enh_val_INDST,Enh_val_INDSCOMP, Enh_val_INDSM2COMP, Enh_val_FSSCOMP,
		MVAL_base, Enh_MVAL_base DOUBLE;


 /*   SELECT
        ah.aid,
		postcode,
		propTypeCategory,
		EPC_ownership,
        thoroughfare,
        lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
        lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
		ind.price AS p,

        CASE WHEN ah.propertyType IS NULL THEN 0 ELSE 1 END AS pd_type_flag,

		CASE WHEN ah.propertyType IS NOT NULL THEN 1
			 WHEN COALESCE(NP_SUBBNAME_F_type2_F, NP_NB1_D_PC_type2_D, NP_NLH1_TM_PC_type2_T) IS NOT NULL THEN 0.4
			 WHEN NP_PD_SUMM0_TO_6_type3_PC IS NOT NULL THEN 0.3
			 ELSE 0.1 END AS CF_data_quality_type,

	   CASE WHEN NUMBER_HABITABLE_ROOMS IS NOT NULL THEN 1
			-- when EA/PORT data is available then 0.5
			WHEN NNPAV_NHR_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_beds,

	   CASE WHEN TOTAL_FLOOR_AREA IS NOT NULL THEN 1
			WHEN NNPAV_M2_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_M2,

	   CASE WHEN ah.propertyType IS NULL THEN NP_PD_SUMM0_TO_9_type4_PC ELSE ah.propertyType END AS pType,
		CASE
			WHEN ten.UD IS NOT NULL THEN ten.UD
			WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP
			ELSE ten.AH
		END AS tenure,
		CASE
			WHEN window_desc.UD IS NOT NULL THEN window_desc.UD
			ELSE window_desc.EPC
		END AS windows,
		ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
		COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
		Indic_year_band_construction AS year_construction,
        CURRENT_ENERGY_RATING,
        latitude4326,
        longitude4326
    FROM
        liqquid_addresshub ah
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
    LEFT JOIN avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
    LEFT JOIN NNPAV_NHR_2 nnpav_nhr ON ah.aid = nnpav_nhr.aid
    LEFT JOIN NNPAV_PropertyType2 nnpav_p ON ah.aid = nnpav_p.aid
    LEFT JOIN NNPAV_M2_2 nnpav_m2 ON ah.aid = nnpav_m2.aid
    LEFT JOIN avm_tenure ten ON ah.aid = ten.aid
    LEFT JOIN avm_window_description window_desc ON ah.aid = window_desc.aid
    LEFT JOIN avm_year_construction yc ON ah.aid = yc.aid
    WHERE
    ah.aid = address_id;*/



# CF_INDSCOMP
# CF_weight_INDSCOMP

-- pull in data on the target property
SELECT
    postcode,
    thoroughfare,
    TYPE1,
    TYPE2,
    TYPE3,
    TYPE4,
    pd_type_flag, -- this is a flag indicating if the type data on the target comes from a PD source
    total_floor_area,
    Beds,
    latitude4326,
    longitude4326,
    EPC_ownership,
    CURRENT_ENERGY_RATING,
    tenure,
    windows,
    year_construction,
    (CF_data_quality_beds + CF_data_quality_beds + CF_data_quality_M2)/3, -- confidence factor on data quality of the target
	CASE
	 WHEN TYPE2='F' THEN
		  final_F_index/init_F_index * p
	 WHEN TYPE2='D' THEN
		  final_D_index/init_D_index * p
	 WHEN TYPE2='S' THEN
		  final_S_index/init_S_index * p
	 WHEN TYPE2='T' THEN
		  final_T_index/init_T_index * p
	 END AS indexed_price,
     number_habitable_rooms
INTO
	pc ,
	street ,
    p_type1 ,
    p_type2 ,
    p_type3 ,
    p_type4 ,
    pd_type_flag_var,
    tfa ,
    n_beds ,
    t_lat ,
    t_lon,
	epcownership ,
    t_epcrating ,
    t_tenure ,
    t_windows ,
    t_year_construction,
    CF_percent_aggreg_data,
    indexed_target_price, nhr
FROM
    (
    SELECT
        ah.aid,
		postcode,
		propTypeCategory,
		EPC_ownership,
        thoroughfare,
        lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
        lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
		ind.price AS p,

        CASE WHEN ah.propertyType IS NULL THEN 0 ELSE 1 END AS pd_type_flag,

		CASE WHEN ah.propertyType IS NOT NULL THEN 1
			 WHEN COALESCE(NP_SUBBNAME_F_type2_F, NP_NB1_D_PC_type2_D, NP_NLH1_TM_PC_type2_T) IS NOT NULL THEN 0.4
			 WHEN NP_PD_SUMM0_TO_6_type3_PC IS NOT NULL THEN 0.3
			 ELSE 0.1 END AS CF_data_quality_type,

	   CASE WHEN NUMBER_HABITABLE_ROOMS IS NOT NULL THEN 1
			-- when EA/PORT data is available then 0.5
			WHEN NNPAV_NHR_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_beds,

	   CASE WHEN TOTAL_FLOOR_AREA IS NOT NULL THEN 1
			WHEN NNPAV_M2_SUMM IS NOT NULL THEN 0.3
			ELSE 0.1 END AS CF_data_quality_M2,

	   CASE WHEN ah.propertyType IS NULL THEN NP_PD_SUMM0_TO_9_type4_PC ELSE ah.propertyType END AS pType,
		CASE
			WHEN ten.UD IS NOT NULL THEN ten.UD
			WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP
			ELSE ten.AH
		END AS tenure,
		CASE
			WHEN window_desc.UD IS NOT NULL THEN window_desc.UD
			ELSE window_desc.EPC
		END AS windows,
		ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
		COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
		Indic_year_band_construction AS year_construction,
		CURRENT_ENERGY_RATING,
		latitude4326,
		longitude4326
    FROM
        liqquid_addresshub ah
    LEFT JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
    LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
    LEFT JOIN avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
    LEFT JOIN NNPAV_NHR_2 nnpav_nhr ON ah.aid = nnpav_nhr.aid
    LEFT JOIN NNPAV_PropertyType2 nnpav_p ON ah.aid = nnpav_p.aid
    LEFT JOIN NNPAV_M2_2 nnpav_m2 ON ah.aid = nnpav_m2.aid
    LEFT JOIN avm_tenure ten ON ah.aid = ten.aid
    LEFT JOIN avm_window_description window_desc ON ah.aid = window_desc.aid
    LEFT JOIN avm_year_construction yc ON ah.aid = yc.aid
    ) s
	LEFT JOIN avm_property_type_matrix ptm ON s.pType = ptm.Type4
    LEFT JOIN liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
WHERE
    aid = address_id
LIMIT 1;


/*SELECT pc ,
	street ,
    p_type1 ,
    p_type2 ,
    p_type3 ,

    p_type4 ,
    pd_type_flag_var,
    tfa ,
    n_beds ,
    t_lat ,
    t_lon,
	epcownership ,
    t_epcrating ,
    t_tenure ,
    t_windows ,
    t_year_construction,
    CF_percent_aggreg_data,
    indexed_target_price, nhr;*/
#SELECT 'a';
-- =========================================================================================================================================================================

-- this section of the script pulls in the nearest postcodes to the target
-- it works by first estimating the density of appropriate comparables in a box with 2km side length centreed on the target property
-- the 2km box is then scaled to contain approximately 250 houses
DROP TEMPORARY TABLE IF EXISTS nearest_postcodes;
CREATE TEMPORARY TABLE nearest_postcodes (postcode VARCHAR(8));
#SELECT 'b';
SET num_comps_in_2km = IFNULL((SELECT SUM(number_of_houses) FROM avm_postcode_type_price_stats_2 WHERE MBRContains(LineString(Point(t_lon + 0.018, t_lat + 0.018), Point(t_lon - 0.018, t_lat - 0.018)), coords)
					AND type1=p_type1 AND Beds = n_beds),0);
#SELECT CONCAT('num comps in 2km = ',num_comps_in_2km);
IF num_comps_in_2km = 0 THEN
	SET search_radius = 0.5;
ELSEIF  num_comps_in_2km > 200 THEN
	SET search_radius = 0.018;
ELSE
	SET search_radius = (250/num_comps_in_2km)*0.018;
END IF;

#SELECT CONCAT('search radius = ', CAST(search_radius as CHAR(50) ) );
INSERT INTO nearest_postcodes SELECT
    postcode
FROM
    (
    SELECT
        postcode
    FROM
        avm_postcode_type_price_stats_2
    WHERE
        MBRCONTAINS(LINESTRING(POINT(t_lon + search_radius, t_lat + search_radius), POINT(t_lon - search_radius, t_lat - search_radius)), coords)
	AND Beds = n_beds
	AND type1 = p_type1
	) t;




# num nearest pc
#SELECT COUNT(*) FROM nearest_postcodes;


SELECT          *
							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((t_lat - latitude4326)*70.25,2) + POWER((t_lon - longitude4326)*42.08,2))*1609.344 AS distance,
									EPC_ownership,
									CURRENT_ENERGY_RATING,
									pp.TransactionDate,
									postcode,
									thoroughfare,
									pp.propertyType AS pp_pType,
									ah.propertyType AS ah_pType,
									CASE WHEN ten.UD IS NOT NULL THEN ten.UD WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP ELSE AH END AS tenure,
									CASE WHEN window_desc.UD IS NOT NULL THEN window_desc.UD ELSE EPC END AS windows,
									NUMBER_HABITABLE_ROOMS AS nhr,
									TOTAL_FLOOR_AREA AS total_floor_area,
									Indic_year_band_construction AS year_construction,
									lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
									lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
									lind.latest_index AS final_O_index, ind.prop_index AS init_O_index,
									ind.price AS p,
									coords
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN (SELECT postcode FROM nearest_postcodes)
								) s
							LEFT JOIN
								avm_property_type_matrix ptm_pp ON s.pp_pType = ptm_pp.Type4
							LEFT JOIN
								avm_property_type_matrix ptm_ah ON s.ah_pType = ptm_ah.Type4
							LEFT JOIN
								liqquid_hab_rooms_est lhre ON s.nhr = lhre.Habitable_Rooms
							WHERE
								Beds=n_beds
							AND
								ptm_ah.TYPE1=p_type1 AND
								p IS NOT NULL AND aid <> address_id
							ORDER BY distance
							LIMIT 100;


/*
SELECT          *
							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((t_lat - latitude4326)*70.25,2) + POWER((t_lon - longitude4326)*42.08,2))*1609.344 AS distance,
								       pp.Price
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode ='al10 0ay') s;

*/




/*SELECT          *
							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((t_lat - latitude4326)*70.25,2) + POWER((t_lon - longitude4326)*42.08,2))*1609.344 AS distance
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN (SELECT postcode FROM nearest_postcodes)
								) s
							ORDER BY distance
							LIMIT 100;*/




END$$
DELIMITER ;



CALL proc(31822306);
#SELECT COUNT(*) FROM liqquid_addresshub WHERE postcode='ox44 7nf';

#SELECT * FROM valdev_4.liqquid_addresshub WHERE aid =31822306;

#SELECT * FROM valdev.liqquid_addresshub WHERE postcode='ox44 7nf' AND buildingName = 'yew tree cottage';


