USE valdev_4;
DROP PROCEDURE IF EXISTS Max_Enhanced_Prices;

-- create view for frequently joined tables involving LR-PP data

DROP VIEW IF EXISTS max_enhanced_price_view_pp;
CREATE VIEW max_enhanced_price_view_pp AS SELECT aid, TYPE1, TYPE2, TYPE3, TYPE4, Beds, postcode, coords,
	total_floor_area,
	CASE WHEN pType='F' THEN
				latest_F/F * Price
                WHEN pType='D' THEN
                latest_D/D * Price
                WHEN pType='S' THEN
                latest_S/S * Price
                WHEN pType='T' THEN
                latest_T/T * Price
                END AS indexed_price
FROM 
(SELECT ah.aid,
			CASE
				WHEN
					propTypeCategory IS NULL
						AND ah.propertyType IS NULL
				THEN
					NP_PD_SUMM0_TO_9_type4_PC
				ELSE ah.propertyType
			END AS pType,
            postcode,
            pp.TransactionDate,
            latest_F,F, latest_D,D,latest_T, T, latest_S, S,
            ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
            COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
            pp.Price AS Price,
            coords
		FROM
			liqquid_addresshub ah
			INNER JOIN liqquid_pp pp ON ah.aid = pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
            INNER JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
			LEFT JOIN NNPAV_NHR nn_nhr ON ah.aid = nn_nhr.aid
            LEFT JOIN NNPAV_PropertyType nn_p ON ah.aid = nn_p.aid
            LEFT JOIN NNPAV_M2_2 nn_m2 ON ah.aid = nn_m2.aid
            LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
		) s
        LEFT JOIN
    avm_property_type_matrix ptm ON s.pType = ptm.Type4
        LEFT JOIN
    liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
    WHERE PRICE IS NOT NULL;


SELECT aid, TYPE1, TYPE2, TYPE3, TYPE4, Beds, postcode, coords,
	total_floor_area,
	CASE WHEN pType='F' THEN
				latest_F/F * Price
                WHEN pType='D' THEN
                latest_D/D * Price
                WHEN pType='S' THEN
                latest_S/S * Price
                WHEN pType='T' THEN
                latest_T/T * Price
                END AS indexed_price
FROM
(SELECT ah.aid,
			CASE
				WHEN
					propTypeCategory IS NULL
						AND ah.propertyType IS NULL
				THEN
					NP_PD_SUMM0_TO_9_type4_PC
				ELSE ah.propertyType
			END AS pType,
            postcode,
            pp.TransactionDate,
            latest_F,F, latest_D,D,latest_T, T, latest_S, S,
            ROUND(COALESCE(NNPAV_NHR_SUMM, NUMBER_HABITABLE_ROOMS)) AS number_habitable_rooms,
            COALESCE(NNPAV_M2_SUMM, TOTAL_FLOOR_AREA) AS total_floor_area,
            pp.Price AS Price,
            coords
		FROM
			liqquid_addresshub ah
			INNER JOIN liqquid_pp pp ON ah.aid = pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
            INNER JOIN liqquid_epc_certs epc ON ah.aid = epc.aid
			LEFT JOIN NNPAV_NHR nn_nhr ON ah.aid = nn_nhr.aid
            LEFT JOIN NNPAV_PropertyType nn_p ON ah.aid = nn_p.aid
            LEFT JOIN NNPAV_M2_2 nn_m2 ON ah.aid = nn_m2.aid
            LEFT JOIN avm_indexed_price ind ON ah.aid = ind.aid
		) s
        LEFT JOIN
    avm_property_type_matrix ptm ON s.pType = ptm.Type4
        LEFT JOIN
    liqquid_hab_rooms_est lhre ON s.number_habitable_rooms = lhre.Habitable_Rooms
    WHERE PRICE IS NOT NULL;

DROP TABLE NNPAV_NHR;
CREATE TABLE NNPAV_NHR LIKE nn_nhr;
INSERT NNPAV_NHR SELECT * FROM nn_nhr;

DROP TABLE NNPAV_PropertyType;
CREATE TABLE NNPAV_PropertyType LIKE nn_type;
INSERT NNPAV_PropertyType SELECT * FROM nn_type;

DROP TABLE NNPAV_M2;
CREATE TABLE NNPAV_M2 LIKE nn_m2;
INSERT NNPAV_M2 SELECT * FROM nn_m2;

SELECT * FROM max_enhanced_price_view_pp;

/*
DROP PROCEDURE IF EXISTS nearest_max_enh_matches;
DELIMITER $$

-- procedure pulls nearest matches based in same street or within fixed radius based on supplied parameters
CREATE PROCEDURE nearest_max_enh_matches(
										IN use_FS BOOL,
                                        IN use_M2 BOOL,
										IN type_level INT,
                                        IN type_code VARCHAR(2),
                                        IN pc VARCHAR(9),
                                        IN lat DOUBLE,
                                        IN lon DOUBLE,
                                        IN distance_in_km DOUBLE,
                                        IN n_beds INT,
                                        IN enh_factor DOUBLE,
                                        OUT address_id INT,
                                        OUT enh_price DECIMAL(10,0))
BEGIN

IF NOT use_FS THEN
	IF pc IS NULL THEN
		SELECT aid, CASE WHEN use_M2  THEN (indexed_price/total_floor_area)*enh_factor ELSE indexed_price*enh_factor END INTO address_id, enh_price FROM
							(SELECT aid,  indexed_price,total_floor_area, ST_Distance_Sphere(Point(lon, lat), coords) AS distance FROM
							max_enhanced_price_view_pp
							WHERE CASE WHEN type_level = 3 THEN TYPE3=type_code
									   WHEN type_level = 2 THEN TYPE2=type_code AND Type3 <> type_code
									   WHEN type_level = 1 THEN TYPE1=type_code
									   END
									   AND Beds=n_beds
							AND
                            indexed_price IS NOT NULL
							AND
							MBRContains(LineString(Point(lon + distance_in_km/111, lat + distance_in_km/111), Point(lon -distance_in_km/111, lat - distance_in_km/111)), coords)
                            AND (total_floor_area IS NOT NULL OR NOT use_M2) -- if use_M2 is true then we only take records with floor area information, otherwise we use all records
							HAVING distance < distance_in_km*1000) t LIMIT 1;
	ELSE
		SELECT aid, CASE WHEN use_M2 THEN (indexed_price/total_floor_area)*enh_factor ELSE indexed_price*enh_factor END  INTO address_id, enh_price FROM
		max_enhanced_price_view_pp
		WHERE CASE WHEN type_level = 3 THEN TYPE3=type_code
				   WHEN type_level = 2 THEN TYPE2=type_code AND Type3 <> type_code
				   WHEN type_level = 1 THEN TYPE1=type_code
				   END
				   AND Beds=n_beds
		AND
			indexed_price IS NOT NULL
		AND
			(total_floor_area IS NOT NULL OR NOT use_M2) -- if use_M2 is true then we only take records with floor area information, otherwise we use all records
		AND
			postcode IN
				(SELECT
					tg1.postcode
				 FROM
					liqquid_thoroughfare_groups tg1
				 INNER JOIN
					liqquid_thoroughfare_groups tg2 ON tg1.thoroughfare = tg2.thoroughfare
				 AND tg1.`group` = tg2.`group`
				 WHERE
					tg2.postcode = pc
				)
			LIMIT 1;

		END IF;

ELSE
		IF pc IS NULL THEN
			SELECT id, price*enh_factor INTO address_id, enh_price FROM
								(SELECT id, price , ST_Distance_Sphere(Point(lon, lat), coords) AS distance FROM
								liqquid_EA_PORT_data
								WHERE CASE WHEN type_level = 3 THEN Type3=type_code
										   WHEN type_level = 2 THEN Type2=type_code AND TYPE3 <> type_code
										   WHEN type_level = 1 THEN Type1=type_code
										   END
										   AND bedrooms=n_beds
								AND
								MBRContains(LineString(Point(lon + distance_in_km/111, lat + distance_in_km/111), Point(lon -distance_in_km/111, lat - distance_in_km/111)), coords)
								HAVING distance < distance_in_km*1000) t LIMIT 1;
		ELSE
			SELECT id, price*enh_factor  INTO address_id, enh_price FROM
			liqquid_EA_PORT_data
			WHERE CASE WHEN type_level = 3 THEN Type3=type_code
					   WHEN type_level = 2 THEN Type2=type_code AND Type3 <> type_code
					   WHEN type_level = 1 THEN Type1=type_code
					   END
					   AND bedrooms=n_beds
			AND
					postcode IN
						(SELECT
							tg1.postcode
						 FROM
							liqquid_thoroughfare_groups tg1
						 INNER JOIN
							liqquid_thoroughfare_groups tg2 ON tg1.thoroughfare = tg2.thoroughfare
						 AND tg1.`group` = tg2.`group`
						 WHERE
							tg2.postcode = pc
						)
					LIMIT 1;

		END IF;
END IF;

END$$




DROP PROCEDURE IF EXISTS Max_Enhanced_Prices;
DELIMITER $$
CREATE PROCEDURE Max_Enhanced_Prices(	IN use_FS BOOL,
										IN use_M2 BOOL,
                                        IN pc VARCHAR(8),
                                        IN n_beds INT,
                                        IN lat DOUBLE,
                                        IN lon DOUBLE,
                                        IN ptype1 VARCHAR(1),
                                        IN ptype2 VARCHAR(1),
                                        IN ptype3 VARCHAR(1),
                                        OUT filter_val DECIMAL
                                        )
BEGIN
DECLARE different_type1 VARCHAR(1);

DROP TEMPORARY TABLE IF EXISTS matched_props;
CREATE TEMPORARY TABLE matched_props(rule INT, id INT, Enhanced_Price DECIMAL(10,0));
-- (1) Same type 3, same beds, same street, EF A (=1.2x)
CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,pc,NULL,NULL,NULL,n_beds,1.2, @address_id, @enh_price);
INSERT INTO matched_props SELECT 1, @address_id, @enh_price WHERE @address_id IS NOT NULL;

-- (2) Same type 2, same beds, same street, EF A (=1.2x)
IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
	CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,pc,NULL, NULL,NULL,n_beds,1.2, @address_id, @enh_price);
    INSERT INTO matched_props SELECT 2,  @address_id, @enh_price WHERE @address_id IS NOT NULL;

    -- (3) Same type 3, same beds, within radius A (=250M), EF A (=1.2x)
    IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
		CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon,0.25,n_beds,1.2, @address_id, @enh_price);
		INSERT INTO matched_props SELECT 3,  @address_id, @enh_price WHERE @address_id IS NOT NULL;

        -- (4) Same type 2, same beds, within radius A (=250M), EF A (=1.2x)
        IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
			CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.25,n_beds,1.2, @address_id, @enh_price);
			INSERT INTO matched_props SELECT 4, @address_id, @enh_price WHERE @address_id IS NOT NULL;

            -- (5) Same type 3, same beds, within radius B (=500M), EF A (=1.2x)
            IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
				CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon, 0.5,n_beds,1.2, @address_id, @enh_price);
				INSERT INTO matched_props SELECT 5, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                -- (6) Same type 2, same beds, within radius B (=500M), EF A (=1.2x)
                 IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
					CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.5,n_beds,1.2, @address_id, @enh_price);
					INSERT INTO matched_props SELECT 6, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                    -- (7-1) Same type 3, + 1 beds, same street, EF B (=1.1x)
                    IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
						CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,pc,NULL,NULL,NULL,n_beds+1,1.1, @address_id, @enh_price);
						INSERT INTO matched_props SELECT 7, @address_id, @enh_price WHERE @address_id IS NOT NULL;

							-- (7-2) Same type 3, - 1 beds, same street, EF B (=1.1x)
							IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
								CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,pc,NULL,NULL,NULL,n_beds-1,1.1, @address_id, @enh_price);
								INSERT INTO matched_props SELECT 7,  @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                -- (8-1) Same type 2, + 1 beds, same street, EF B (=1.1x)
                                IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
									CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,pc,NULL,NULL,NULL,n_beds+1,1.1, @address_id, @enh_price);
									INSERT INTO matched_props SELECT 8, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                    -- (8-2) Same type 2, - 1 beds, same street, EF B (=1.1x)
									IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
										CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,pc,NULL,NULL,NULL,n_beds-1,1.1, @address_id, @enh_price);
										INSERT INTO matched_props SELECT 8, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                        -- (9-1) Same type 3, + 1 beds, within radius A (=250M), EF B (=1.1x)
                                        IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
											CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon,0.25,n_beds+1,1.1, @address_id, @enh_price);
											INSERT INTO matched_props SELECT 9,  @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                            -- (9-2) Same type 3, - 1 beds, within radius A (=250M), EF B (=1.1x)
                                            IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
												CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon,0.25,n_beds-1,1.1, @address_id, @enh_price);
												INSERT INTO matched_props SELECT 9, @address_id, @enh_price WHERE @address_id IS NOT NULL;

													-- (10-1) Same type 2, + 1 beds, within radius A (=250M), EF B (=1.1x)
													IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
														CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.25,n_beds+1,1.1, @address_id, @enh_price);
														INSERT INTO matched_props SELECT 10,  @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                        -- (10-2) Same type 2, - 1 beds, within radius A (=250M), EF B (=1.1x)
                                                        IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
															CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.25,n_beds-1,1.1, @address_id, @enh_price);
															INSERT INTO matched_props SELECT 10, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                            -- (11-1) Same type 3, + 1 beds, within radius B (=500M), EF B (=1.1x)
                                                            IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon,0.5,n_beds+1,1.1, @address_id, @enh_price);
																INSERT INTO matched_props SELECT 11, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                -- (11-2) Same type 3, - 1 beds, within radius B (=500M), EF B (=1.1x)
																IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																	CALL nearest_max_enh_matches(use_FS, use_M2, 3,ptype3,NULL,lat,lon,0.5,n_beds-1,1.1, @address_id, @enh_price);
																	INSERT INTO matched_props SELECT 11, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                    -- (12-1) Same type 2, + 1 beds, within radius B (=500M), EF B (=1.1x)
                                                                    IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																		CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.5,n_beds+1,1.1, @address_id, @enh_price);
																		INSERT INTO matched_props SELECT 12, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                        -- (12-2) Same type 2, - 1 beds, within radius B (=500M), EF B (=1.1x)
																		IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																			CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,0.5,n_beds-1,1.1, @address_id, @enh_price);
																			INSERT INTO matched_props SELECT 12, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                            -- (13-1) Same type 2, + 1 beds, within radius C (=2000M), EF B (=1.1x)
                                                                            IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																				CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,2,n_beds+1,1.1, @address_id, @enh_price);
																				INSERT INTO matched_props SELECT 13, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                -- (13-2) Same type 2, - 1 beds, within radius C (=2000M), EF B (=1.1x)
																				IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																					CALL nearest_max_enh_matches(use_FS, use_M2, 2,ptype2,NULL,lat,lon,2,n_beds-1,1.1, @address_id, @enh_price);
																					INSERT INTO matched_props SELECT 13, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                    -- (14) Same type 1, same beds, within radius C (=2000M), EF B (=1.1x)
																					IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																						CALL nearest_max_enh_matches(use_FS, use_M2, 1,ptype1,NULL,lat,lon,2,n_beds,1.1, @address_id, @enh_price);
																						INSERT INTO matched_props SELECT 14, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                        -- (15) Different type 1, same beds, within radius C (=2000M), EF B (=1.1x)
                                                                                        IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																							SELECT CASE WHEN ptype1 = "H" THEN "F" WHEN ptype1="F" THEN "H" ELSE NULL END INTO different_type1;
																							CALL nearest_max_enh_matches(use_FS, use_M2, 1,different_type1,NULL,lat,lon,2,n_beds,1.1, @address_id, @enh_price);
																							INSERT INTO matched_props SELECT 15, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                            -- (16-1) Same type 1, + 1 beds, within radius C (=2000M), EF B (=1.1x)
																							IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																								CALL nearest_max_enh_matches(use_FS, use_M2, 1,ptype1,NULL,lat,lon,2,n_beds+1,1.1, @address_id, @enh_price);
																								INSERT INTO matched_props SELECT 16, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                                -- (16-2) Same type 1, - 1 beds, within radius C (=2000M), EF B (=1.1x)
                                                                                                IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																									CALL nearest_max_enh_matches(use_FS, use_M2, 1,ptype1,NULL,lat,lon,2,n_beds-1,1.1, @address_id, @enh_price);
																									INSERT INTO matched_props SELECT 16, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                                    -- (17-1) Different type 1, + 1 beds, within radius C (=2000M), EF B (=1.1x)
                                                                                                    IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																										SELECT CASE WHEN ptype1 = "H" THEN "F" WHEN ptype1="F" THEN "H" ELSE NULL END INTO different_type1;
																										CALL nearest_max_enh_matches(use_FS, use_M2, 1,different_type1,NULL,lat,lon,2,n_beds+1,1.1, @address_id, @enh_price);
																										INSERT INTO matched_props SELECT 17, @address_id, @enh_price WHERE @address_id IS NOT NULL;

                                                                                                         -- (17-2) Different type 1, - 1 beds, within radius C (=2000M), EF B (=1.1x)
                                                                                                        IF (SELECT COUNT(DISTINCT id) FROM matched_props) < 3 THEN
																											SELECT CASE WHEN ptype1 = "H" THEN "F" WHEN ptype1="F" THEN "H" ELSE NULL END INTO different_type1;
																											CALL nearest_max_enh_matches(use_FS, use_M2, 1,different_type1,NULL,lat,lon,2,n_beds-1,1.1, @address_id, @enh_price);
																											INSERT INTO matched_props SELECT 17, @address_id, @enh_price WHERE @address_id IS NOT NULL;

																										END IF;

																									END IF;

																								END IF;

																							END IF;

																						END IF;

																					END IF;

																				END IF;

																			END IF;

																		END IF;


																	END IF;

																END IF;

															END IF;

														END IF;

													END IF;

											END IF;

										END IF;

									END IF;

								END IF;

							END IF;

					END IF;

                END IF;

			END IF;

		END IF;

	END IF;

END IF;

SET filter_val = (SELECT AVG(Enhanced_Price) FROM matched_props);
END$$
DELIMITER ;
#@FILTER=
CALL Max_Enhanced_Prices(FALSE, TRUE, 'PO8 9PB', 5, 50.9145011902, -1.0115513802, 'H', 'D', 'D', @FILTER);
SELECT @FILTER;

CALL Max_Enhanced_Prices(TRUE, FALSE, 'CH44 4DR', 3, 53.4134407043, -3.0400469303, 'H', 'T', 'T', @FILTER);
SELECT @FILTER;

CALL Max_Enhanced_Prices(TRUE, FALSE, 'CH44 4DR', 3, 53.4134401000, -3.0400469000, 'H', 'T', 'T', @FILTER);
SELECT @FILTER;
-- valdev_2 = 123125

#SELECT * FROM valdev_4.liqquid_addresshub WHERE address1Line LIKE '41 KINGSLEY%';

#SHOW;
*/



#SELECT * FROM information_schema.PROCESSLIST;

SELECT COUNT(*) FROM nn_nhr;
SELECT COUNT(*) FROM valdev.NNPAV_NHR where NNPAV_NHR_SUMM        <> 0;
SELECT COUNT(*) FROM valdev_4.nn_nhr where NNPAV_NHR_SUMM         <> 0;
SELECT COUNT(*) FROM valdev_4.nn_nhr where NNPAV_NHR_SUMM IS NOT NULL;



