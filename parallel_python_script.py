#! /home/ubuntu/anaconda3/envs/py373/bin/python

DATABASE = 'avm'
#from sql_functions import *
import pymysql
from sqlalchemy import create_engine
import pandas as pd
import mysql.connector
import numpy as np


DATABASE = 'avm'
LISTT_type = list(range(1, 100, 1))

from multiprocessing import Pool, cpu_count
import pymysql
import time

processes = 30
print('processes = '+str(processes))

def parallel_calc_type(func):
    with Pool(processes) as p:
        ret_list = p.map(func, LISTT_type)
    return ret_list

def foo_1_types(offset):
    print('starting ' + str(offset) + ' calc ...')
    db = pymysql.connect(
        host='liqquid-r5-xlarge.cokbmcletl0d.eu-west-2.rds.amazonaws.com',
        user='admin',
        password='s4erjd9edid9odb',
        port=3306,
        database=DATABASE,
        charset='utf8'
    )
    with db.cursor() as cur:
        cur.execute('CALL calc({offset});'.format(offset=offset))
    results = cur.fetchall()
    db.commit()
    db.close()
    print(str(offset) + ' calc')
    return results



tic = time.time()
a=parallel_calc_type(foo_1_types)
toc = time.time()
print(toc-tic)
