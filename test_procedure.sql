
use avm;


DROP PROCEDURE IF EXISTS calc;
DELIMITER $$
CREATE PROCEDURE calc(address_id INT)
BEGIN
SELECT
    *, 1 AS rank
    FROM
    (SELECT
        aid,
								address1line,
								CASE WHEN TOTAL_FLOOR_AREA=0 THEN 0 ELSE 1 END AS has_epc_m2,
								(POWER(POWER(GREATEST( (distance/1609.344),(1/15) ),2), -1) + 50*(CASE WHEN thoroughfare='kingsley road' THEN 1 ELSE 0 END) + 50*(CASE WHEN postcode='ch44 4dr' THEN 1 ELSE 0 END)) AS distance_points,
                                (30*(CASE WHEN EPC_ownership='private' THEN 1 ELSE 0 END)) AS EPC_ownership_points,
                                GREATEST((1-DATEDIFF(CURDATE(),TransactionDate)/3650)*300,0) AS transac_timing_points,
                                IFNULL((SELECT points FROM avm_tenure_att_points WHERE tenure_of_target = 'f' and tenure_of_comp=tenure),0) AS tenure_points,
                                IFNULL((SELECT points FROM avm_window_description_points WHERE target_windows_desc = 'fully double glazed' and comp_windows_desc=windows),0) AS windows_points,
                                CASE WHEN ABS(1-( 87.0 / total_floor_area )) > 0.2 THEN 0 ELSE ((( 0.2 - ABS(1-( 87.0 / total_floor_area ))) / 0.2 ) * 100) END AS M2_points,
								IFNULL((SELECT points FROM avm_EPC_rating_points WHERE target_EPC_rating = 'c' and comp_EPC_rating=CURRENT_ENERGY_RATING),0) AS EPC_rating_points,
								IFNULL((SELECT points FROM avm_year_construction_points WHERE target_year_construction = '1900-1929' and comp_year_construction=year_construction),0) AS year_construction_points,
								CASE WHEN 1 = 1 THEN CASE WHEN ptm_ah.TYPE4='TM' THEN 100 WHEN ptm_ah.TYPE3='T' THEN 80 WHEN ptm_ah.TYPE2='T' THEN 75 WHEN ptm_ah.TYPE1='H' THEN 0 ELSE 0 END ELSE 0 END AS type_points,
							    total_floor_area,
								CASE
								   WHEN ptm_pp.TYPE2='F' THEN
									    final_F_index/init_F_index * p
								   WHEN ptm_pp.TYPE2='D' THEN
									    final_D_index/init_D_index * p
								   WHEN ptm_pp.TYPE2='S' THEN
									    final_S_index/init_S_index * p
								   WHEN ptm_pp.TYPE2='T' THEN
									    final_T_index/init_T_index * p
								   WHEN ptm_pp.TYPE2='O' THEN
									    final_O_index/init_O_index * p
								   END AS indexed_price


							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER((53.4130882 - latitude4326)*70.25,2) + POWER((-3.0402038 - longitude4326)*42.08,2))*1609.344 AS distance,
									EPC_ownership,
									CURRENT_ENERGY_RATING,
									pp.TransactionDate,
									postcode,
									thoroughfare,
									pp.propertyType AS pp_pType,
									ah.propertyType AS ah_pType,
									CASE WHEN ten.UD IS NOT NULL THEN ten.UD WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP ELSE AH END AS tenure,
									CASE WHEN window_desc.UD IS NOT NULL THEN window_desc.UD ELSE EPC END AS windows,
									NUMBER_HABITABLE_ROOMS AS nhr,
									TOTAL_FLOOR_AREA AS total_floor_area,
									Indic_year_band_construction AS year_construction,
									lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
									lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
									lind.latest_index AS final_O_index, ind.prop_index AS init_O_index,
									ind.price AS p,
									coords
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN ('ch44 6jj','ch44 7as','ch44 6nq','ch44 6nd','ch44 6jg','ch44 6jy','ch44 6le','ch44 6lf','ch44 6lg','ch44 7ea','ch44 7dz','ch44 6lq','ch44 6lu','ch44 6lx','ch44 6pw','ch44 6pn','ch44 7en','ch44 7ld','ch44 7ew','ch44 6lr','ch44 7lb','ch44 7ee','ch44 6lb','ch44 7eh','ch44 7eq','ch44 7et','ch44 7es','ch44 7dp','ch41 3lu','ch41 3qb','ch44 7ar','ch44 6nh','ch44 7dx','ch44 7aj','ch44 6rb','ch44 7dy','ch41 3qd','ch44 6ne','ch41 3ly','ch44 7aw','ch44 6rd','ch44 6jz','ch41 3py','ch41 3pr','ch41 3pb','ch41 3pz','ch41 3qh','ch41 3np','ch41 3pq','ch41 3lh','ch41 3jy','ch41 1dp','ch44 8bu','ch44 8bt','ch44 8bx','ch44 8by','ch44 6qb','ch44 6qa','ch44 6qg','ch44 8bz','ch44 6qq','ch44 6qe','ch44 8da','ch44 8dj','ch44 6pz','ch44 8db','ch44 8dy','ch44 8dn','ch44 6qd','ch44 8dr','ch44 8ee','ch44 6py','ch44 8eg','ch44 8dl','ch44 8du','ch44 8bg','ch44 6qf','ch44 6ls','ch44 8be','ch44 6lt','ch44 8bd','ch44 8dp','ch44 8dw','ch44 9dw','ch44 8hq','ch44 8es','ch44 8bs','ch44 8ds','ch44 9dn','ch44 8ef','ch44 8bj','ch44 7ez','ch44 9dp','ch44 7hh','ch44 7hg','ch44 7hl','ch44 7hj','ch44 7eu','ch44 8hf','ch44 7ey','ch44 7ha','ch44 8eq','ch44 8hg','ch44 9dq','ch44 0bj','ch44 9dr','ch44 0bn','ch44 8ej','ch44 7hn','ch44 7hs','ch44 8af','ch44 9dt','ch44 0de','ch44 9ad','ch44 9ds','ch44 9du','ch44 9es','ch44 8bl','ch44 8bn','ch44 8bw','ch44 8bp','ch44 8ah','ch44 0dg','ch44 0db','ch44 8br','ch44 8ew','ch44 8ar','ch44 0ej','ch44 0bz','ch44 0ga','ch44 8er','ch44 9ax','ch44 0bp','ch44 8hy','ch44 8ha','ch44 0dd','ch44 0ez','ch44 0el','ch44 8ae','ch44 0br','ch44 9ab','ch44 0bw','ch44 0bl','ch44 0ds','ch44 8ag','ch44 0et','ch44 0ee','ch44 9aa','ch44 0dw','ch44 1ed','ch44 9an','ch44 0ea','ch44 0dp','ch44 0bh','ch44 0dn','ch44 0dq','ch44 9bh','ch44 0du','ch44 0es','ch44 9ah','ch44 0dr','ch44 9aj','ch44 1dq','ch44 0dt','ch44 0bq','ch44 0bg','ch44 1ej','ch44 0er','ch45 7pw','ch44 0dy','ch44 1ee','ch45 7pn','ch44 0bd','ch44 1eh','ch44 0dx','ch44 0be','ch44 1eq','ch44 0eb','ch44 0ax','ch44 1ds','ch44 1dr','ch44 0ay','ch44 1ef','ch44 0at','ch44 1dt','ch44 1el','ch41 3sg','ch41 3sd','ch41 3sl','ch41 3sn','ch41 3rz','ch41 3sr','ch41 3sp','ch41 4hj','ch41 4hn','ch41 4hs','ch41 4hl','ch41 4hg','ch41 4hq','ch41 4pa','ch41 4nx','ch41 4ny','ch41 4ht','ch41 4hu','ch41 4hx','ch41 4nz','ch41 4hz','ch41 4ha','ch41 4jb','ch44 9al','ch44 7hw','ch44 9et','ch44 9dx','ch44 9en','ch44 9hb','ch44 9dy','ch44 9de','ch44 9dh','ch44 4hg','ch44 9ew','ch44 9ar','ch44 9ap','ch44 9er','ch44 9ay','ch44 9ep','ch44 9el','ch44 9eu','ch44 9dz','ch44 4aj','ch44 9ej','ch44 9eq','ch44 9eb','ch44 9bq','ch44 9df','ch44 9bg','ch44 9eg','ch44 9ea','ch44 9be','ch44 9bp','ch44 9eh','ch44 9az','ch44 4ba','ch44 9dj','ch44 4az','ch44 9ba','ch44 9bb','ch44 9bl','ch44 9bj','ch44 9bd','ch44 4al','ch44 4ap','ch44 4bb','ch44 9dl','ch44 9bw','ch44 4bg','ch44 4bh','ch44 4bd','ch44 9br','ch44 4be','ch44 4bq','ch44 9bs','ch44 9bz','ch44 4bw','ch44 4ay','ch44 4bz','ch44 4bn','ch44 9by','ch44 4bj','ch44 4dj','ch44 4br','ch44 4dg','ch44 4bs','ch44 4ar','ch44 9da','ch44 4an','ch44 9bu','ch44 4bp','ch44 4ax','ch44 4au','ch44 4at','ch44 4dh','ch44 4bl','ch44 9bx','ch44 4dp','ch44 9db','ch44 4by','ch44 4dw','ch44 4dl','ch44 4dq','ch44 4dn','ch44 4bu','ch44 4as','ch44 4da','ch44 4ds','ch44 4bt','ch44 4dr','ch44 4aw','ch44 4hs','ch44 4bx','ch44 4du','ch44 4dt','ch44 4dx','ch41 3jr','ch41 3lq','ch41 3hx','ch41 8el','ch41 8ef','ch41 8br','ch41 8bl','ch41 8fn','ch41 8bb','ch41 8at','ch41 8fj','ch41 8ab','ch41 8fl','ch41 8as','ch41 8ar','ch41 8ap','ch45 7qy','ch45 7pt','ch45 7qx','ch45 7ps','ch45 7px','ch45 7pb','ch45 1hn','ch45 7pd','ch45 7rh','ch45 7ql','ch45 7qw','ch45 1hu','ch45 7sj','ch45 1hf','ch45 1hb','ch45 7nh','ch45 7sh','ch45 7na','ch45 7nd','ch45 7nb','ch45 1ha','ch45 7nl','ch45 7nj','ch45 7nr','ch45 7ns','ch45 7nt','ch45 7nu','ch45 7nx','ch45 1jf','ch45 1jb','ch45 7ny','ch45 7nf','ch45 7ng','ch45 1jn','ch45 7np','ch45 1jp','ch45 5dh','ch45 5ej','ch45 5ds','ch45 5dr','ch44 0ba','ch44 1dd','ch44 0aw','ch44 1dn','ch44 0ed','ch45 7ly','ch44 1dy','ch44 0an','ch44 1dp','ch44 0al','ch44 0eh','ch44 0ad','ch44 1dj','ch44 0as','ch44 0ar','ch44 0aj','ch44 1du','ch44 1dl','ch44 0ah','ch44 0ap','ch44 0aq','ch44 1dg','ch45 7lx','ch44 1dw','ch44 1ay','ch45 7lz','ch44 0ag','ch44 1dh','ch44 1ax','ch45 7lu','ch44 1bg','ch44 1de','ch44 1au','ch45 7hh','ch44 1az','ch44 1bb','ch44 1bs','ch44 1as','ch44 1be','ch44 1bp','ch44 1bh','ch44 1ba','ch44 1br','ch44 1at','ch44 1bd','ch44 1bj','ch45 7ne','ch45 7lp','ch44 1ag','ch45 7lq','ch44 1an','ch44 1bl','ch44 1bt','ch44 1al','ch45 7lf','ch44 5ty','ch44 5tx','ch45 7lw','ch44 1ap','ch44 1aq','ch45 7ll','ch44 1aj','ch44 1ab','ch44 5ul','ch45 4ja','ch44 5un','ch45 4jb','ch44 5uh','ch44 1ad','ch44 5ur','ch45 7pu','ch45 7qr','ch45 7qt','ch45 1ht','ch45 7qp','ch45 7qh','ch45 1nj','ch45 7qj','ch45 7qq','ch45 1jl','ch45 5du','ch45 5eg','ch45 5ep','ch45 5eb','ch45 5eq','ch45 5ee','ch45 5er','ch45 5ed','ch45 5es','ch45 7lj','ch45 7ln','ch45 7lg','ch45 5dw','ch45 7ld','ch45 7le','ch44 1aa','ch44 5ua','ch45 5de','ch44 5us','ch45 5df','ch44 5xe','ch45 5dd','ch44 5uj','ch45 4ls','ch45 4lz','ch45 4lt','ch44 5ub','ch45 4lx','ch45 4ly','ch45 4lr','ch45 4lu','ch45 4lg','ch44 5xd','ch44 5xb','ch44 5uy','ch45 4lb','ch45 4lw','ch44 5rd','ch45 4ln','ch44 5ux','ch45 4qu','ch45 4qy','ch45 4qx','ch45 4nb','ch45 4qz','ch45 4re','ch45 4rf','ch45 4ra','ch45 4rb','ch44 5ra','ch45 4pq','ch44 5rb','ch44 5xq','ch44 5uz','ch44 5xg','ch45 4ph','ch44 5rs','ch45 4lj','ch44 5xa','ch44 5rr','ch45 4rd','ch45 4pj','ch45 4rh','ch44 5rh','ch45 4nd','ch44 5rp','ch45 4rl','ch45 4pg','ch45 4ll','ch45 4rq','ch44 5rq','ch44 5rl','ch44 5rj','ch45 4rg','ch44 2ae','ch45 4rr','ch44 5rf','ch45 4rn','ch45 4pl','ch45 4sd','ch45 4sf','ch45 4pn','ch45 4se','ch45 5jt','ch45 5ja','ch45 5hh','ch45 5hz','ch45 5ht','ch45 5hd','ch45 5hf','ch45 5he','ch45 5hl','ch45 5hy','ch45 5hq','ch45 5hg','ch45 5hn','ch45 5ha','ch45 5hu','ch45 5hs','ch45 4qq','ch45 4pz','ch45 6ul','ch45 4qa','ch45 4px','ch45 4pw','ch45 4pu','ch45 4qj','ch45 4qh','ch45 4py','ch45 6ur','ch45 4qb','ch45 4qd','ch45 4pt','ch45 6uh','ch45 6us','ch45 6ut','ch45 4ps','ch45 6xp','ch45 6tq','ch45 0jf','ch45 6tg','ch45 6xh','ch45 6xa','ch45 6xf','ch45 6uy','ch45 6xj','ch45 6uz','ch45 6xn','ch45 6xw','ch45 6uu','ch44 4ex','ch44 5up','ch44 4dy','ch44 4ae','ch44 4eg','ch44 4de','ch44 4eq','ch44 4aq','ch44 4ad','ch44 4ag','ch44 4db','ch44 4ea','ch44 4af','ch44 5xf','ch44 4el','ch44 4ef','ch44 4dz','ch44 5ut','ch44 5uu','ch44 4ep','ch44 4eu','ch44 4ej','ch44 4et','ch44 4dd','ch44 4eh','ch44 4ee','ch44 4ew','ch44 4eb','ch44 4df','ch44 4en','ch44 5aa','ch44 4es','ch44 4ed','ch44 4er','ch44 5uq','ch44 5rn','ch44 5tb','ch44 5sb','ch44 5rw','ch44 5tg','ch44 5sa','ch44 5ys','ch44 5rx','ch44 5tq','ch44 5te','ch44 5tf','ch44 5rt','ch44 5sy','ch44 5rg','ch44 5rz','ch44 5sz','ch44 5ru','ch44 5ss','ch44 5sx','ch44 5ry','ch44 5se','ch44 5su','ch44 3bh','ch44 5sf','ch44 5sd','ch44 5sq','ch44 3bn','ch44 5sg','ch44 5sr','ch44 5sj','ch44 3aa','ch44 5sh','ch44 5sl','ch44 3bw','ch44 3aj','ch44 3ab','ch44 3ad','ch44 5sp','ch44 5sn','ch44 3bj','ch44 3ae','ch44 3af','ch44 3aq','ch44 3dj','ch44 3ap','ch44 3au','ch44 3an','ch44 3ar','ch44 3aw','ch44 3al','ch44 3at','ch44 3bd','ch44 3bb','ch44 3ej','ch44 3as','ch44 3eh','ch44 3ay','ch44 3dr','ch44 3ds','ch44 3ax','ch44 3dz','ch44 3be','ch44 3dt','ch45 4rw','ch45 4rs','ch45 4rp','ch44 3bu','ch44 3bt','ch45 4rt','ch44 3da','ch44 2ar','ch44 2at','ch45 4ru','ch45 4sq','ch45 4sg','ch44 3dl','ch44 3dp','ch45 4sb','ch44 2ax','ch44 3db','ch44 3dx','ch45 4rz','ch44 2au','ch44 2az','ch44 3dw','ch44 3ea','ch44 3dn','ch44 2ay','ch45 4rx','ch44 3de','ch44 2hq','ch44 3dy','ch44 3eb','ch44 2ey','ch44 3ed','ch44 2bg','ch45 6tb','ch44 3eg','ch44 2ag','ch44 2ba','ch44 2bs','ch44 3ef','ch44 2be','ch44 2ad','ch44 2bh','ch44 2bq','ch44 2bd','ch45 6te','ch44 2bj','ch45 6tf','ch44 2bu','ch44 2bt','ch44 2bl','ch44 2br','ch44 3bg','ch41 4hw','ch41 4jh','ch41 4jg','ch41 4jd','ch41 4jj','ch41 4ja','ch41 4hb','ch41 0as','ch41 0aq','ch41 0ar','ch41 0ap','ch41 0ep','ch41 0ed','ch41 0er','ch41 0df','ch41 0el','ch41 0ej','ch41 0eh','ch41 0ag','ch41 0bw','ch41 0aa','ch41 0bn','ch41 0an','ch41 0bq','ch41 0dd','ch41 0bg','ch41 0ad','ch41 0bj','ch41 0ah','ch41 0bh','ch41 0da','ch41 0be','ch41 0bl','ch41 8js','ch41 8jt','ch41 0bd','ch41 0aw','ch41 0br','ch41 8jr','ch41 0bx','ch41 8hp','ch41 0bb','ch41 8hr','ch41 0bu','ch41 0aj','ch41 8ju','ch41 0ba','ch41 0ab','ch41 8jj','ch41 8ff','ch41 8jg','ch41 0al','ch41 0bt','ch41 8hw','ch41 0bz','ch41 0ax','ch41 0bs','ch41 0az','ch41 0by','ch41 8jp','ch41 7aa','ch41 8hy','ch41 0au','ch41 0at','ch41 8je','ch41 8jf','ch41 8hz','ch41 7ep','ch41 8jl','ch41 8jq','ch41 7af','ch41 7aj','ch41 7al','ch41 7ew','ch41 7az','ch41 7ap','ch41 0db','ch41 0dj','ch41 8al','ch41 8ad','ch41 8aj','ch41 8ba','ch41 8ed','ch41 8ag','ch41 8bx','ch41 8eb','ch41 8du','ch41 8ea','ch41 8dp','ch41 8dx','ch41 8dj','ch41 8da','ch41 8dg','ch41 8dh','ch41 8db','ch41 8ae','ch41 8dn','ch41 8dq','ch41 8bz','ch41 8jy','ch41 8dz','ch41 8ah','ch41 8af','ch41 8de','ch41 8df','ch43 7rw','ch43 7rr','ch43 7rp','ch44 4dr')
								) s
							LEFT JOIN
								avm_property_type_matrix ptm_pp ON s.pp_pType = ptm_pp.Type4
							LEFT JOIN
								avm_property_type_matrix ptm_ah ON s.ah_pType = ptm_ah.Type4
							LEFT JOIN
								liqquid_hab_rooms_est lhre ON s.nhr = lhre.Habitable_Rooms
							WHERE
								Beds=3.0
							AND
								ptm_ah.TYPE1='H' AND
								p IS NOT NULL AND aid <> 21813088
							ORDER BY distance
							LIMIT 10) p;

END$$
DELIMITER ;


#SELECT CONCAT('KILL ',ID, ';') FROM information_schema.PROCESSLIST;
#CALL calc(1);
#SELECT * from information_schema.PROCESSLIST;

